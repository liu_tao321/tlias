package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
//@ComponentScan(basePackages = {
//        "com.example",
//        "com.itheima"})
//@Import(YourImportSelector.class)
public class SpringBootMybatisQuickstartApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMybatisQuickstartApplication.class, args);
    }

}
