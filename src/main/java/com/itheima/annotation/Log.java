package com.itheima.annotation;

import java.lang.annotation.*;

/**
 * 自定义Log注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {
}