package com.itheima.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.jopo
 * @className: PageBean
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/15 14:35
 * @version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBean {
    private Long total; //总记录数
    private List<?> rows; //当前页数据列表


}
