package com.itheima.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.common.config
 * @className: AliConfig
 * @author: Eric
 * @description: 阿里的配置类
 * @date: 2023/7/16 16:04
 * @version: 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "ali.oss")
public class AliConfig {
    private String endpiont;
    private String accessKeyId;
    private String accessKeySecret;
    private String buckeName;

    private List<String> hobby;

    private User user;

    @Data
    public static class User {
        private String name;
        private Integer age;
    }
}
