package com.itheima.common.config;

import com.itheima.common.interceptor.DemoInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.common.config
 * @className: WebConfig
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/18 15:27
 * @version: 1.0
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private DemoInterceptor demoIntercepor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(demoIntercepor)
                //拦截所有
                .addPathPatterns("/**")
                //排除登录请求
                .excludePathPatterns("/login","/upload");
    }
}
