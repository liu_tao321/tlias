package com.itheima.common.exceptionhandler;

import com.itheima.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.common.exceptionhandler
 * @className: GlobalExceptionHandler
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/18 16:21
 * @version: 1.0
 */
@Slf4j
//@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    public Result exception(Exception exception) {
        log.error(exception.getMessage(), exception);
        exception.printStackTrace();
        return Result.error("未知异常");
    }


    @ExceptionHandler(RuntimeException.class)
    public Result exception(RuntimeException e) {
        log.error(e.getMessage(), e);
        e.printStackTrace();
        return Result.error("不好意思，出错啦！");
    }
}
