package com.itheima.common.interceptor;

import com.alibaba.fastjson.JSON;
import com.itheima.common.Result;
import com.itheima.common.util.JwtUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.common.interceptor
 * @className: DemoInterceptor
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/18 15:29
 * @version: 1.0
 */
@Configuration
public class DemoInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("Token");
        if (!StringUtils.hasText(token)) {
            //失败，不放行
            Result notLogin = Result.error("NOT_LOGIN");
            //对象变成json串
            String json = JSON.toJSONString(notLogin);
            //设置响应头，表示响应的数据类型是JSON
            response.setContentType("application/json;charset=utf-8");
            //将数据写进响应体中
            response.getWriter().write(json);
            return false;
        }

        try {
            JwtUtils.parseJWT(token);
            return true;
        } catch (Exception e) {
            //失败，不放行
            Result notLogin = Result.error("NOT_LOGIN");
            //对象变成json串
            String json = JSON.toJSONString(notLogin);
            response.setContentType("application/json;charset=utf-8");
            //响应
            response.getWriter().write(json);
        }
        return false;
    }
}
