package com.itheima.common.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.itheima.common.config.AliConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Component
public class AliOSSUtils {
    //    private String endpoint = "https://oss-cn-shanghai.aliyuncs.com";
//    private String accessKeyId = "LTAI5tRJK4un7GGQ7sud6rYm";
//    private String accessKeySecret = "hLBGXPsI4zjKYRQ5nPGClhRNgG9dVB";
//    private String bucketName = "mine-dev";
//
//    @Value("${ali.oss.endpoint:默认值}")
//    private String endpoint;
//    @Value("${ali.oss.accessKeyId}")
//    private String accessKeyId;
//    @Value("${ali.oss.accessKeySecret}")
//    private String accessKeySecret;
//    @Value("${ali.oss.bucketName}")
//    private String bucketName;
//    @Value("${ali.oss.hobby}")
//    private List<String> hobby;
    @Autowired
    private AliConfig aliConfig;
//    @Autowired
//    private AliOSSProperties aliOSSProperties;

    /**
     * 实现上传图片到OSS
     */
    public String upload(MultipartFile multipartFile) throws IOException {

        String endpoint = aliConfig.getEndpiont();
        String accessKeyId = aliConfig.getAccessKeyId();
        String accessKeySecret = aliConfig.getAccessKeySecret();
        String bucketName = aliConfig.getBuckeName();
        // 获取上传的文件的输入流
        InputStream inputStream = multipartFile.getInputStream();

        // 避免文件覆盖
        String originalFilename = multipartFile.getOriginalFilename();
        String fileName = UUID.randomUUID().toString() + originalFilename.substring(originalFilename.lastIndexOf("."));

        //上传文件到 OSS
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        ossClient.putObject(bucketName, fileName, inputStream);

        //文件访问路径
        String url = endpoint.split("//")[0] + "//" + bucketName + "." + endpoint.split("//")[1] + "/" + fileName;

        // 关闭ossClient
        ossClient.shutdown();
        return url;// 把上传到oss的路径返回
    }
}