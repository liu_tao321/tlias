package com.itheima.controller;

import com.itheima.common.PageBean;
import com.itheima.common.Result;
import com.itheima.pojo.Classes;
import com.itheima.service.ClassesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.controller
 * @className: Classes
 * @author: Eric
 * @description: 班级管理
 * @date: 2023/7/20 15:13
 * @version: 1.0
 */
@Slf4j
@RequestMapping("classes")
@RestController
public class ClassesController {
    @Autowired
    private ClassesService classesService;

    /**
     * 添加班级
     *
     * @param classes
     * @return
     */
    @PostMapping
    public Result addclass(@RequestBody Classes classes) {
        log.info("新增班级，classes:{}", classes);
        classesService.addclass(classes);
        return Result.success();
    }

    /**
     * 根据班级id删除班级
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteclass(@PathVariable Integer id) {
        log.info("删除部门id:{}", id);
        classesService.deleteclasses(id);
        return Result.success();
    }

    /**
     * 根据ID获取班级信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result listclassesById(@PathVariable String id) {
        log.info("根据ID查询班级:{}", id);
        Integer i = Integer.parseInt(id);
        Classes classes = classesService.listclassesById(i);
        return Result.success(classes);
    }

    /**
     * 根据ID修改班级信息
     *
     * @param classes
     * @return
     */
    @PutMapping
    public Result updateclassesById(@RequestBody Classes classes) {
        log.info("修改班级信息:{}", classes);
        classesService.updateclassesById(classes);
        return Result.success();
    }

    /**
     * 分页显示全部班级
     *
     * @param name
     * @param begin
     * @param end
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping
    public Result selectclassesPage(
            int page, int pageSize, String name,
            @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate begin,
            @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end) {
        PageBean classesPageBean = classesService.selectclassesPage(name,
                begin, end, page, pageSize);
        return Result.success(classesPageBean);
    }
}
