package com.itheima.controller;

import com.itheima.common.Result;
import com.itheima.pojo.Dept;
import com.itheima.service.DeptService;
import com.itheima.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.controller
 * @className: DeptController
 * @author: Eric
 * @description: 部门管理
 * @date: 2023/7/15 10:24
 * @version: 1.0
 */
@Slf4j
@RequestMapping("/depts")
@RestController
public class DeptController {
    @Autowired
    private DeptService deptService;
    @Autowired
    private EmpService empService;

    /**
     * 获取部门数据
     *
     * @return
     */
    @GetMapping
    public Result list() {
        List<Dept> depts = deptService.list();
        return Result.success(depts);
    }

    /**
     * 根据id删除部门
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public Result deleteById(@PathVariable Integer id) {
        log.info("删除部门id：{}", id);
        deptService.removrById(id);
        empService.removrByDeptId(id);
        return Result.success();
    }

    /**
     * 添加部门信息的方法
     *
     * @param dept
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Dept dept) {
        log.info("新增部门：{}", dept);
        deptService.add(dept);
        return Result.success();
    }

    /**
     * 修改部门信息
     *
     * @param dept
     * @return
     */
    @PutMapping
    public Result updateById(@RequestBody Dept dept) {
        log.info("修改部门信息：{}", dept);
        deptService.update(dept);
        return Result.success();
    }

    /**
     * 根据ID查询部门
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result listById(@PathVariable Integer id) {
        log.info("根据ID查询部门：{}", id);
        Dept dept = deptService.queryById(id);
        return Result.success(dept);
    }

}
