package com.itheima.controller;

import com.itheima.common.PageBean;
import com.itheima.common.Result;
import com.itheima.pojo.Emp;
import com.itheima.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * EmpController
 *
 * @author liutao
 * @version 1.0
 * @description 员工管理
 * @date 2023/4/28 12:59
 */
@RequestMapping("/emps")
@RestController
@Slf4j
public class EmpController {
    private final EmpService empService;

    public EmpController(EmpService empService) {
        this.empService = empService;
    }

    /**
     * 查看所有员工信息
     *
     * @param name
     * @param gender
     * @param begin
     * @param end
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping
    public Result selectPage(
            String name, Integer gender,
            @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate begin,
            @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        PageBean empPageBean = empService.selectPage(name,
                gender, begin, end, page, pageSize);
        return Result.success(empPageBean);
    }

    /**
     * 删除员工信息（可批量删除）
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/{ids}")
    public Result deleteById(@PathVariable List<Integer> ids) {

        empService.delete(ids);
        return Result.success();
    }

    /**
     * 添加员工信息
     *
     * @param emp
     * @return
     */
    @PostMapping
    public Result save(@RequestBody Emp emp) {
        log.info("新增员工, emp:{}", emp);
        empService.save(emp);
        return Result.success();
    }

    /**
     * 根据id查看信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result queryById(@PathVariable Integer id) {
        log.info("根据ID查询员工：{}", id);
        Emp emp = empService.queryById(id);
        return Result.success(emp);
    }

    /**
     * 修改信息
     *
     * @param emp
     * @return
     */

    @PutMapping
    public Result update(@RequestBody Emp emp) {
        log.info("修改员工信息：{}", emp);
        empService.update(emp);
        return Result.success();
    }

}
