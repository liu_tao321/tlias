package com.itheima.controller;

import com.itheima.common.Result;
import com.itheima.common.util.JwtUtils;
import com.itheima.pojo.Emp;
import com.itheima.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Objects;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.controller
 * @className: LoginController
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/16 17:04
 * @version: 1.0
 */
@RestController
@Slf4j
public class LoginController {
    @Autowired
    private EmpService empService;

    /**
     * 登录
     *
     * @param emp
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody Emp emp) {
        Emp empExist = empService.login(emp);
        if (Objects.isNull(empExist)) {
            return Result.error("登陆失败");
        }
        //生成
        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", empExist.getId());
        String jwt = JwtUtils.generateJwt(params);
        return Result.success(jwt);
    }
}
