package com.itheima.controller;

import com.itheima.common.PageBean;
import com.itheima.common.Result;
import com.itheima.pojo.Student;
import com.itheima.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.controller
 * @className: StudentController
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/20 15:13
 * @version: 1.0
 */
@Slf4j
@RequestMapping("/student")
@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;

    /**
     * 添加学生
     *
     * @param student
     * @return
     */
    @PostMapping
    public Result addstudent(@RequestBody Student student) {
        log.info("添加学生，student:{}", student);
        studentService.addstudent(student);
        return Result.success();
    }

    /**
     * 删除学生（可以批量删除）
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/{ids}")
    public Result deleteById(@PathVariable List<Integer> ids) {
        studentService.deletestudentById(ids);
        return Result.success();
    }

    /**
     * 根据id查询学生
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result querystudentById(@PathVariable Integer id) {
        log.info("根据ID查询学生:{}", id);
        Student student = studentService.querystudentById(id);
        return Result.success(student);
    }


    /**
     * updatestudent
     * 根据学生id修改学生信息
     *
     * @param student 学生
     * @return {@link Result}
     */
    @PutMapping
    public Result updatestudent(@RequestBody Student student) {
        log.info("修改学生信息:{}", student);
        studentService.updatestudent(student);
        return Result.success();
    }

    /**
     * 分页查询学生
     *
     * @param page
     * @param pageSize
     * @param name
     * @param highestDegree
     * @param classesId
     * @param studentNumber
     * @return
     */
    @GetMapping
    public Result selectstudentpage(
            Integer page, Integer pageSize,
            String name, Integer highestDegree,
            Integer classesId, String studentNumber) {
        log.info("分页查找");
        PageBean studentBean = studentService.selectstudentpage(page, pageSize, name, highestDegree, classesId, studentNumber);
        return Result.success(studentBean);
    }

    /**
     * 修改扣分
     *
     * @param student
     * @return
     */
    @PutMapping("/deductions")
    public Result deduct(@RequestBody Student student) {
        log.info("扣分调用");
        studentService.deduct(student);
        return Result.success();
    }

    /**
     * 获取班级比例
     *
     * @return
     */
    @GetMapping("studentreport")
    public Result getClassNumber() {
        log.info("学生数据统计");
        List list = studentService.getStudentReport();
        return Result.success(list);
    }

    /**
     * 获取班级比例
     *
     * @return
     */
    @GetMapping("studenttable")
    public Result getSex() {
        log.info("学生数据统计");
        List list = studentService.getStudentTable();
        return Result.success(list);
    }
}
