package com.itheima.controller;

import com.itheima.common.Result;
import com.itheima.common.util.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.controller
 * @className: UploadController
 * @author: Eric
 * @description: 文件上传
 * @date: 2023/7/16 10:13
 * @version: 1.0
 */
@Slf4j
@RestController
public class UploadController {

    @Autowired
    private AliOSSUtils aliOSSUtils;

    /**
     * 文件上传
     *
     * @param image
     * @param name
     * @param age
     * @return
     */
    @PostMapping("/upload")
    public Result upload(MultipartFile image, String name, Integer age) throws IOException {

        log.info("接收到的参数：{},{},{}", image, name, age);

        String url = aliOSSUtils.upload(image);
        return Result.success(url);

    }
}
