package com.itheima.filter;

import com.alibaba.fastjson.JSON;
import com.itheima.common.Result;
import com.itheima.common.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

//定义一个类，实现一个标准的Filter过滤器的接口
@Slf4j
@Component
@WebFilter
public class LoginCheckFilter implements Filter {
    @Override //初始化方法, 只调用一次
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init 初始化方法执行了");
    }

    @Override //拦截到请求之后调用, 调用多次
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //前置：强制转换为http协议的请求对象、响应对象 （转换原因：要使用子类中特有方法）
        HttpServletRequest request = (HttpServletRequest) servletRequest;

//1.获取请求url
        String url = request.getRequestURI().toString();
//2.判断请求url中是否包含login，如果包含，说明是登录操作，放行
        if (url.contains("login")) {
            filterChain.doFilter(servletRequest,servletResponse);//放行请求
            return;//结束当前方法的执行
        }
        //3.获取请求头中的令牌（token）
        String token = request.getHeader("Token");
        //5.解析token，如果解析失败，返回错误结果（未登录）
        try {
            JwtUtils.parseJWT(token);
        } catch (Exception e) {
            String s = JSON.toJSONString(Result.error("NOT_LOGIN"));
            servletResponse.getWriter().write(s);
            return;
        }
        //6.放行
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override //销毁方法, 只调用一次
    public void destroy() {
        System.out.println("destroy 销毁方法执行了");
    }


}