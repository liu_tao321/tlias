package com.itheima.mapper;

import com.github.pagehelper.Page;
import com.itheima.pojo.Classes;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDate;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.mapper
 * @className: ClassesMapper
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/20 15:36
 * @version: 1.0
 */
@Mapper
public interface ClassesMapper {
    //添加班级
    void insert(Classes classes);

    void deleteclasses(Integer id);


    void updateById(Classes classes);

    Classes selectById(Integer id);

    Page<Classes> selectClasses(String name, LocalDate begin, LocalDate end);

}
