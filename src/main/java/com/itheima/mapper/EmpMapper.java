package com.itheima.mapper;

import com.github.pagehelper.Page;
import com.itheima.pojo.Emp;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;

/**
 * EmpDao
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/4/25 16:46
 */
@Mapper
public interface EmpMapper {
    /**
     * 分页查询得到员工数据
     *
     * @param name
     * @param gender
     * @param begin
     * @param end
     * @param index
     * @param pageSize
     * @return
     */
    List<Emp> selectPage(String name, Short gender, LocalDate begin, LocalDate end, Integer index, Integer pageSize);

    /**
     * 查询所有员工信息
     *
     * @param name
     * @param gender
     * @param begin
     * @param end
     * @return List<Emp>
     */
    Page<Emp> selectList(String name, Integer gender, LocalDate begin, LocalDate end, Integer index, Integer pageSize);

    /**
     * 分页查询得到的 数据总数量
     *
     * @param name
     * @param gender
     * @param begin
     * @param end
     * @param index
     * @param pageSize
     * @return
     */
    Long selectCount(String name, Short gender, LocalDate begin, LocalDate end, Integer index, Integer pageSize);

    /**
     * 批量删除员工
     *
     * @param
     */
    void delete(List<Integer> ids);

    /**
     * 根据ID修改员工
     *
     * @param emp
     */
    void update(Emp emp);

    /**
     * 根据ID查询员工
     *
     * @param id
     * @return
     */
    @Select("select * from emp where id = #{id}")
    Emp selectById(Integer id);

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @Select("select * " +
            "from emp " +
            "where username=#{username} and password=#{password}")
    Emp login(String username, String password);

    /**
     * 新增员工
     *
     * @param emp
     */
    @Insert("insert into emp (username, name, gender, image, job, entrydate, dept_id, create_time, update_time) " +
            "values (#{username}, #{name}, #{gender}, #{image}, #{job}, #{entrydate}, #{deptId}, #{createTime}, #{updateTime});")
    void insert(Emp emp);

    void deleteByDeptId(Integer id);

    void removrByDeptId(Integer id);
}
