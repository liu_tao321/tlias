package com.itheima.mapper;

import com.github.pagehelper.Page;
import com.itheima.pojo.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentMapper {

    void insert(Student student);

    void delete(List<Integer> ids);

    Student selectById(Integer id);

    Page<Student> selectList(String name, String studentNumber, Integer highestDegree, Integer classesId, Integer page, Integer pageSize);

    void updateStudent(Student student);

    Page<Student> selectStudent(String name, Integer highestDegree, Integer classesId, String studentNumber);

    void deduct(Student student);
    
    List getStudentReport();

    List getStudentTable();

    Integer getDisciplineScore(Integer id);
}
