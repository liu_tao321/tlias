package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @projectName: tlias-springboot-project
 * @package: com.itheima.tliasspringbootproject.pojo
 * @className: Class
 * @author: Eric
 * @description: 班级
 * @date: 2023/7/20 14:37
 * @version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Classes {
    private Integer id;
    private String name;
    private String classesNumber;
    private String startTime;
    private String finishTime;
    private Integer empId;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
