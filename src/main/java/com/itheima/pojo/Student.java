package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @projectName: tlias-springboot-project
 * @package: com.itheima.tliasspringbootproject.pojo
 * @className: Student
 * @author: Eric
 * @description: 学生
 * @date: 2023/7/20 14:37
 * @version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private Integer id;
    private String name;
    private String studentNumber;
    private Integer gender;
    private String phone;
    private Integer highestDegree;
    private Integer classesID;
    private Integer disciplineTimes;
    private Integer disciplineScore;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private String classesName;
}
