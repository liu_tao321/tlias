package com.itheima.service;

import com.itheima.common.PageBean;
import com.itheima.pojo.Classes;

import java.time.LocalDate;

public interface ClassesService {

    /**
     * addclass
     *添加
     * @param classes 类
     */
    void addclass(Classes classes);

    /**
     * deleteclasses
     *删除
     * @param id id
     */
    void deleteclasses(Integer id);

    /**
     * updateclasses通过id
     *修改
     * @param classes 类
     */
    void updateclassesById(Classes classes);

    /**
     * listclasses通过id
     *
     * @param i 我
     * @return {@link Classes}
     */
    Classes listclassesById(Integer i);

    /**
     * selectclasses页面
     *分页
     * @param name     名字
     * @param begin    开始
     * @param end      结束
     * @param page     页面
     * @param pageSize 页面大小
     * @return {@link PageBean}
     */
    PageBean selectclassesPage(String name, LocalDate begin, LocalDate end, int page, int pageSize);

}
