package com.itheima.service;

import com.itheima.pojo.Dept;

import java.util.List;

public interface DeptService {

    void removrById(Integer id);

    List<Dept> list();

    void update(Dept dept);

    void add(Dept dept);

    Dept queryById(Integer id);
}
