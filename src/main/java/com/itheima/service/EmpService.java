package com.itheima.service;

import com.itheima.common.PageBean;
import com.itheima.pojo.Emp;

import java.time.LocalDate;
import java.util.List;

/**
 * EmpService
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/4/28 13:00
 */
public interface EmpService {

    PageBean selectPage(String name, Integer gender, LocalDate begin, LocalDate end, Integer page, Integer pageSize);

    void delete(List<Integer> ids);


    void update(Emp emp);

    Emp queryById(Integer id);

    Emp login(Emp emp);

    void save(Emp emp);

    void removrByDeptId(Integer id);
}
