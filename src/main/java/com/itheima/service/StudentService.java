package com.itheima.service;

import com.itheima.common.PageBean;
import com.itheima.pojo.Student;

import java.util.List;

public interface StudentService {
    void addstudent(Student student);

    void deletestudentById(List<Integer> ids);

    Student querystudentById(Integer id);

    void updatestudent(Student student);

    PageBean selectstudentpage(Integer page, Integer pageSize, String name, Integer highestDegree, Integer classesId, String studentNumber);

    void deduct(Student student);

    List getStudentReport();

    List getStudentTable();
}
