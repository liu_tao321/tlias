package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.common.PageBean;
import com.itheima.mapper.ClassesMapper;
import com.itheima.pojo.Classes;
import com.itheima.service.ClassesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.service.impl
 * @className: ClassesServiceImpl
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/20 15:19
 * @version: 1.0
 */
@Slf4j
@Service
public class ClassesServiceImpl implements ClassesService {
    @Autowired
    private ClassesMapper classesMapper;

    @Override
    public void addclass(Classes classes) {
        classes.setCreateTime(LocalDateTime.now());
        classes.setUpdateTime(LocalDateTime.now());
        classesMapper.insert(classes);
    }

    @Override
    public void deleteclasses(Integer id) {
        classesMapper.deleteclasses(id);
    }

    @Override
    public Classes listclassesById(Integer id) {
        Classes classes = classesMapper.selectById(id);
        return classes;
    }

    @Override
    public PageBean selectclassesPage(String name, LocalDate begin, LocalDate end, int page, int pageSize) {
        PageHelper.startPage(page, pageSize);
        Page<Classes> p = classesMapper.selectClasses(name, begin, end);

        PageBean pageBean = new PageBean();
        pageBean.setTotal(p.getTotal());
        pageBean.setRows(p.getResult());
        return pageBean;
    }

    @Override
    public void updateclassesById(Classes classes) {
        classes.setUpdateTime(LocalDateTime.now());
        classesMapper.updateById(classes);
    }


}
