package com.itheima.service.impl;

import com.itheima.pojo.Dept;
import com.itheima.mapper.DeptMapper;
import com.itheima.mapper.EmpMapper;
import com.itheima.service.DeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.service.impl
 * @className: DeptServiceImpl
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/15 10:26
 * @version: 1.0
 */
@Slf4j
@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptMapper deptMapper;
    @Autowired
    private EmpMapper empMapper;

    @Override
    public List<Dept> list() {
        List<Dept> deptList = deptMapper.list();
        return deptList;
    }


    @Override
    public void update(Dept dept) {
        dept.setUpdateTime(LocalDateTime.now());
        deptMapper.update(dept);
    }

    @Override
    public void add(Dept dept) {
        //在这里校验部门已存在
        //抛出异常
        Integer count = deptMapper.selectCountByName(dept.getName());
        if (count > 0) {
            throw new RuntimeException("部门已经存在啦！");
        }
        dept.setCreateTime(LocalDateTime.now());
        dept.setUpdateTime(LocalDateTime.now());
        deptMapper.insert(dept);
    }

    @Override
    public Dept queryById(Integer id) {
        Dept dept = deptMapper.selectById(id);
        return dept;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removrById(Integer id) {
        //调用持久层删除功能
        deptMapper.deleteById(id);
        //模拟：异常发生
        empMapper.deleteByDeptId(id);
    }
}
