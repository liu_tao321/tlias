package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.pojo.Emp;
import com.itheima.common.PageBean;
import com.itheima.mapper.EmpMapper;
import com.itheima.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * EmpServiceImpl
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/4/28 13:00
 */
@Slf4j
@Service
public class EmpServiceImpl implements EmpService {
    @Autowired
    private EmpMapper empMapper;

    @Override
    public PageBean selectPage(String name,
                               Integer gender,
                               LocalDate begin, LocalDate end,
                               Integer page, Integer pageSize) {

        //1.员工信息- 分页
        //使用分页插件
        //1.不需要计算count
        //2.不需要分页了
        PageHelper.startPage(page, pageSize);
        //1.员工信息-分页
        Page<Emp> empPage = empMapper.selectList(name, gender, begin, end, page, pageSize);

        PageBean empPageBean = new PageBean();
        empPageBean.setRows(empPage.getResult());
        empPageBean.setTotal(empPage.getTotal());
        return empPageBean;
    }

    @Override
    public void delete(List<Integer> ids) {

        empMapper.delete(ids);
    }


    @Override
    public void update(Emp emp) {
        emp.setUpdateTime(LocalDateTime.now());
        empMapper.update(emp);
    }

    @Override
    public Emp queryById(Integer id) {
        Emp emp = empMapper.selectById(id);
        emp.setPassword("");
        return emp;
    }

    @Override
    public Emp login(Emp emp) {
        String username = emp.getUsername();
        String password = emp.getPassword();
        return empMapper.login(username, password);
    }

    @Override
    public void save(Emp emp) {
        emp.setPassword("123456");
        emp.setCreateTime(LocalDateTime.now());
        emp.setUpdateTime(LocalDateTime.now());
        //调用添加方法
        empMapper.insert(emp);
    }

    @Override
    public void removrByDeptId(Integer id) {
        empMapper. removrByDeptId(id);
    }
}
