package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.annotation.Log;
import com.itheima.common.PageBean;
import com.itheima.mapper.StudentMapper;
import com.itheima.pojo.Student;
import com.itheima.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @projectName: springboot-day10
 * @package: com.itheima.service.impl
 * @className: StudentServiceImpl
 * @author: Eric
 * @description: TODO
 * @date: 2023/7/20 15:22
 * @version: 1.0
 */
@Slf4j
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Log
    @Override
    public void addstudent(Student student) {
        student.setCreateTime(LocalDateTime.now());
        student.setUpdateTime(LocalDateTime.now());
        studentMapper.insert(student);
    }

    @Log
    @Override
    public void deletestudentById(List<Integer> ids) {
        studentMapper.delete(ids);
    }

    @Override
    public Student querystudentById(Integer id) {
        Student student=studentMapper.selectById(id);
        return student;
    }

    @Log
    @Override
    public void updatestudent(Student student) {
        student.setUpdateTime(LocalDateTime.now());
        studentMapper.updateStudent(student);
    }

    @Override
    public PageBean selectstudentpage(Integer page, Integer pageSize, String name, Integer highestDegree, Integer classesId, String studentNumber) {
        PageHelper.startPage(page, pageSize);
        Page<Student> p = studentMapper.selectStudent(name, highestDegree, classesId, studentNumber);
        PageBean pageBean = new PageBean();
        pageBean.setRows(p.getResult());
        pageBean.setTotal(p.getTotal());
        return pageBean;
    }

    @Log
    @Override
    public void deduct(Student student) {
        studentMapper.deduct(student);
        if (studentMapper.getDisciplineScore(student.getId()) >= 40) {
            List<Integer> f= new ArrayList<>();
            f.add(student.getId());
            deletestudentById(f);
        }
    }

    @Override
    public List getStudentReport() {
        return studentMapper.getStudentReport();
    }

    @Override
    public List getStudentTable() {
        return studentMapper.getStudentTable();
    }
}
