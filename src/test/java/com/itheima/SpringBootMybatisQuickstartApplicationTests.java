package com.itheima;

import com.itheima.controller.DeptController;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import java.util.Date;
import java.util.HashMap;


@SpringBootTest
class SpringBootMybatisQuickstartApplicationTests {
    @Test
    void testJWGen() {
        HashMap<String, Object> param = new HashMap<>();
        param.put("id", 1);
        param.put("name", "zhangsan");
        String jwt = Jwts.builder()
                .setClaims(param)
                .signWith(SignatureAlgorithm.HS256, "1324135464")
                .setExpiration(new Date(new Date().getTime() + 300 * 1000))
                .compact();
        System.out.println(jwt);
    }

    @Test
    void testSec() {
        String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiemhhbmdzYW4iLCJpZCI6MSwiZXhwIjoxNjg5NjQ4NzEyfQ.FzEdJ4MdgsostU1eOBRchw98olTCQ2G_ScDQZiUy8R4";
        Claims body = Jwts.parser()
                .setSigningKey("1324135464")
                .parseClaimsJws(jwt)
                .getBody();
        Object id = body.get("id");
        Object name = body.get("name");
        System.out.println(id);
        System.out.println(name);
    }

    //IOC容器
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testApplicationContext() {
        //1.根据name获取bean DeptController
        DeptController deptController = (DeptController) applicationContext.getBean("deptController");
        System.out.println(deptController);

        //2.根据类型获取bean DeptController
        DeptController deptController1 = applicationContext.getBean(DeptController.class);
        System.out.println(deptController1);

        //3.根据name获取bean（带类型转换）
        DeptController deptController2 = applicationContext.getBean("deptController", DeptController.class);
        System.out.println(deptController2);

    }



}
